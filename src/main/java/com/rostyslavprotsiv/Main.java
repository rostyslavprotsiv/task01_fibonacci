
/*
 * Main class
 *
 * Version 1.0.0
 *
 * Created by Protsiv Rostyslav 30.03.2019
 */


package com.rostyslavprotsiv;

import com.rostyslavprotsiv.action.RangeAction;
import com.rostyslavprotsiv.exceptions.RangeActionLogicalException;
/**
 * @version 1.0.0 30 March 2019
 * @author Rostyslav Protsiv
 * @since 2019-03-30
 *
 */

public final class Main {

    /**
     * <p>This constructor must be not called.</p>
     */
    private Main() {

    }

    /**
     * @param args Arguments of main method
     */
    public static void main(final String[] args) {

        RangeAction ra = new RangeAction();
        try {
            ra.setIntervalFromKeyboard();
        } catch (RangeActionLogicalException rale) {
            rale.printStackTrace();
            System.err.println("You entered not good interval (first element "
                    + "must be smaller than second element)");
        }
        ra.outOddEven();
        ra.outSumOddEven();
        ra.createFibonacci();
        ra.generatePercentageOddEvenFibonacci();

    }
}
