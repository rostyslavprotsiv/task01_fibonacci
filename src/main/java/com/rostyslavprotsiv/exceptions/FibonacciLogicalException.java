/*
 * FibonacciLogicalException class created to message us about a problem
 * in a logic of work with objects of Fibonacci class
 *
 *  version 1.0.0
 *
 *  Created by Protsiv Rostyslav 30.03.2019
 */

package com.rostyslavprotsiv.exceptions;

/**
 *
 * @version 1.1.0 30 March 2019
 * @author Rostyslav Protsiv
 * @since 2019-03-30
 */
public class FibonacciLogicalException extends Exception {

    public FibonacciLogicalException() {
    }

    public FibonacciLogicalException(final String s) {
        super(s);
    }

    public FibonacciLogicalException(final String s,
                                     final Throwable throwable) {
        super(s, throwable);
    }

    public FibonacciLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public FibonacciLogicalException(final String s,
                                     final Throwable throwable,
                                     final boolean b, final boolean b1) {
        super(s, throwable, b, b1);
    }
}
