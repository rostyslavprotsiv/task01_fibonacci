/*
 * RangeActionLogicalException class created for right
 * using RangeAction class
 *
 * version 1.0.0
 *
 * Created by Protsiv Rostyslav 30.03.2019
 *
 */

package com.rostyslavprotsiv.exceptions;


/**
 *
 * @version 1.1.0 01 April 2019
 * @author Rostyslav Protsiv
 * @since 2019-03-30
 */
public class RangeActionLogicalException extends Exception {
    public RangeActionLogicalException() {
    }

    public RangeActionLogicalException(final String s) {
        super(s);
    }

    public RangeActionLogicalException(final String s,
                                       final Throwable throwable) {
        super(s, throwable);
    }

    public RangeActionLogicalException(final Throwable throwable) {
        super(throwable);
    }

    public RangeActionLogicalException(final String s,
                                       final Throwable throwable,
                                       final boolean b,
                                       final boolean b1) {
        super(s, throwable, b, b1);
    }
}
