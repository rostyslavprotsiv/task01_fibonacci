/*
 * Menu class
 *
 * Version 1.0.0
 *
 * Created by Protsiv Rostyslav 30.03.2019
 *
 * This view will help user to communicate with the program
 *
 */


package com.rostyslavprotsiv.views;

import java.util.Scanner;

/**
 * <h1>User Menu</h1>
 * The Menu  implements an application that
 * simply displays menu to the standard output.
 * <p>
 *
 * @version 1.1.0 30 March 2019
 * @author Rostyslav Protsiv
 * @since 2019-03-30
 */

public class Menu {
    /**
     *
     * @version 1.1.0 30 March 2019
     * @since 2019-03-30
     * @return Bottom edge and top edge of our variety
     */
    public final int[] enterTwoNumbers() {
        Scanner scan = new Scanner(System.in);
        int[] edges = new int[2];    //Bottom edge and top edge
        System.out.println("Please enter the interval : ");
        System.out.println("Enter the bottom edge: ");
        edges[0] = scan.nextInt();
        System.out.println("Enter the top edge: ");
        edges[1]  = scan.nextInt();
        return edges;
    }

    /**
     *
     * <p>This function will ask to enter the size of set[N] for building
     * Fibonacci numbers.</p>
     * @version 1.1.0 31 March 2019
     * @since 2019-03-30
     * @return return the size of future set
     */
    public final int enterSizeOfSet() {
        Scanner scan = new Scanner(System.in);
        final int defaultValueOfSize = 5;
        int size = defaultValueOfSize;    //size of future set
        boolean userWant;
        System.out.println("Do you want to input size of your future "
                 + "Fibonacci range ? (input \"true\" if you want and "
                + "\"false\" in a case if you don't)");
        userWant = scan.nextBoolean();
        if (userWant) {
            System.out.println("Please enter the size : ");
            size = scan.nextInt();
            return size;
        } else {
            return size;
        }
    }

    /**
     *
     * @version 1.1.0 31 March 2019
     * @since 2019-03-30
     * @param odd Odd number
     */
    public final void outOdd(final int odd) {
        System.out.println("Odd = " + odd);
    }

    /**
     *
     * @version 1.1.0 31 March 2019
     * @since 2019-03-30
     * @param even Even number
     */
    public final void outEven(final int even) {
        System.out.println("Even = " + even);
    }

    /**
     *
     * @version 1.1.0 31 March 2019
     * @since 2019-03-30
     * @param sumOfOdd sum of odd numbers
     * @param sumOfEven sum of even numbers
     */
    public final void outSumOddEven(final int sumOfOdd, final int sumOfEven) {
        System.out.println("Sum of odd numbers : " + sumOfOdd);
        System.out.println("Sum of even numbers : " + sumOfEven);
    }

    /**
     *<p>This method will output percentage of Fibonacci numbers.</p>
     * @version 1.1.0 31 March 2019
     * @since 2019-03-30
     * @param percOdd Percents of odd numbers
     * @param percEven Percents of even numbers
     */
    public final void printPercentageFibonacciOddEven(final double percOdd,
                                                      final double percEven) {
        System.out.println("Percents of Odd numbers : " + percOdd + "%");
        System.out.println("Percents of Even numbers : " + percEven + "%");
    }

}
