/*
 * Fibonacci class
 *
 * version 1.0.0
 *
 * Created by Protsiv Rostyslav 31.03.2019
 *
 */

package com.rostyslavprotsiv.entity;

import com.rostyslavprotsiv.exceptions.FibonacciLogicalException;
import java.util.LinkedHashSet;

/**
 * <p>This class realize an entity that named Fibonacci range.</p>
 * @version 1.1.0 31 March 2019
 * @author Rostyslav Protsiv
 * @since 2019-03-30
 *
 */
public class Fibonacci {
    /**
     * Excatly our range of Fibonacci.
     *
     */
    private LinkedHashSet<Integer> range;

    public Fibonacci(final LinkedHashSet<Integer> range) {
        this.range = range;
    }

    public Fibonacci() {

    }

    /**
     *
     *
     * @param size This is size of Fibonacci range
     * @param f1 This is the first element
     * @param f2 This is the second element
     * @throws FibonacciLogicalException Own exception
     */
    public Fibonacci(final int size, final int f1, final int f2)
            throws FibonacciLogicalException {
        if (checkSize(size) && checkF1F2(f1, f2)) {
            generateRange(size, f1, f2);
        } else {
            throw new FibonacciLogicalException();
        }
    }

    public final LinkedHashSet<Integer> getRange() {
        return range;
    }

    public final void setRange(final int size, final int f1, final int f2)
            throws FibonacciLogicalException {
        if (checkSize(size) && checkF1F2(f1, f2)) {
            generateRange(size, f1, f2);
        } else {
            throw new FibonacciLogicalException();
        }
    }

    /**
     * <p>This method we use to check whether size
     * is normal for our HashSet.</p>
     * @param size This is size of range
     * @return true if it is validated
     */
    private boolean checkSize(final int size) {
        final int maxSizeOfRange = 9999;
        return (size > 0) && (size <= maxSizeOfRange);
    }

    /**
     * <p>This method we use to check if first element is
     * less than second element in Fibonacci range.</p>
     * @param f1 first element in range Fibonacci
     * @param f2 second element in range Fibonacci
     * @return true if f1 <= f2
     */
    private boolean checkF1F2(final int f1, final int f2) {
        return f1 <= f2;
    }

    /**
     * This function is created for generating range
     * Fibonacci with given parameters.
     *
     * @param size This is inputed size of range Fibonacci
     * @param f1 First member of range
     * @param f2 Second member of range
     */
    private void generateRange(final int size, final int f1, final int f2) {
        final int f1AndF2 = 2;
        range = new LinkedHashSet<>(size);
        int current = f2;
        int previous = f1;
        int sumFib;
        range.add(f1);
        range.add(f2);
        for (int i = f1AndF2; i < size; i++) {
            sumFib = current + previous;
            previous = current;
            current = sumFib;
            range.add(sumFib);

        }
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Fibonacci other = (Fibonacci) obj;
        if (range == null) {
            if (other.range != null) {
                return false;
            }
        } else if (!range.equals(other.range)) {
            return false;
          }
        return true;
    }

    @Override
    public final int hashCode() {
        return range.hashCode();
    }

    @Override
    public final String toString() {
        return getClass().getName() + " @range : " + range.toString();
    }
}
