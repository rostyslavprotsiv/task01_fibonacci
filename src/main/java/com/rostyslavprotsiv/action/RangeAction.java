/*
 * RangeAction class
 *
 *
 * Version 1.0.0
 *
 *
 * Created by Protsiv Rostyslav 30.03.2019
 *
 *
 */

package com.rostyslavprotsiv.action;

import com.rostyslavprotsiv.exceptions.FibonacciLogicalException;
import com.rostyslavprotsiv.views.Menu;
import com.rostyslavprotsiv.entity.Fibonacci;
import com.rostyslavprotsiv.exceptions.RangeActionLogicalException;

import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * <p>This class is created for executing operations with numbers.</p>
 * @version 1.1.0 31 March 2019
 * @author Rostyslav Protsiv
 * @since 2019-03-30
 *
 */
public class RangeAction {
    private final Menu menu = new Menu();
    private Fibonacci fib; //Fibonacci range
    /**
     * This is the first number of range.
     */
    private int firstNumber;

    /**
     * This is the second number of range.
     */
    private int secondNumber;


    private boolean checkInterval(final int firstNumber,
                                  final int secondNumber) {
        return firstNumber <= secondNumber;
    }

    /**
     * This method is created for setting firstNumber and
     * secondNumber from keyboard.
     *
     * @throws RangeActionLogicalException Exception about logic
     * of RangeAction class
     *
     */
     public final void setIntervalFromKeyboard()
            throws RangeActionLogicalException {
        int[] temporary = menu.enterTwoNumbers();
        if (checkInterval(temporary[0], temporary[1])) {
            firstNumber = temporary[0];
            secondNumber = temporary[1];
        } else {
            throw new RangeActionLogicalException();
        }
    }

    /**
     *
     * This method prints odd numbers from start to the end
     * of interval and even from end to start.
     *
     */
    public final void outOddEven() {
        for (int i = firstNumber; i <= secondNumber; i++) {
            if ((i % 2) != 0) {
                menu.outOdd(i);

            }
        }
        for (int i = secondNumber; i >= firstNumber; i--) {
            if ((i % 2) == 0) {
                menu.outEven(i);

            }
        }
    }

    /**
     *
     * This method prints the sum of odd and even numbers.
     */
    public final void outSumOddEven() {
        int sumOfOdd = 0;
        int sumOfEven = 0;
        for (int i = firstNumber; i <= secondNumber; i++) {
            if ((i % 2) != 0) {
                sumOfOdd += i;

            } else {
                sumOfEven += i;
            }
        }
        menu.outSumOddEven(sumOfOdd, sumOfEven);
    }

    /**
     *
     * This method builds Fibonacci numbers: F1 will be the biggest odd number
     * and F2 – the biggest even number, user can enter the size of set (N).
     */
    public final void createFibonacci() {
        int sizeOfFibonacci = menu.enterSizeOfSet();
        int theBiggestOdd;
        int theBiggestEven;
        final int forSmallerOdd = 2; // supporting variable
        if ((secondNumber % 2) == 0) {
            theBiggestEven = secondNumber;
            theBiggestOdd = secondNumber - 1;
        } else {
            /*
             * We do this, because all the biggest  odds must be less
             * than all the biggest evens
             */
            theBiggestOdd = secondNumber - forSmallerOdd;
            theBiggestEven = secondNumber - 1;
        }
        try {
            fib = new Fibonacci(sizeOfFibonacci, theBiggestOdd, theBiggestEven);
            System.out.println(fib);
        } catch (FibonacciLogicalException fle) {
            fle.printStackTrace();
            System.err.println("Error with creating object \"Fibonacci\"");
        }
    }

    /**
     *
     * This method will generate and print percentage of odd
     * and even Fibonacci numbers.
     */
    public final void generatePercentageOddEvenFibonacci() {
        LinkedHashSet<Integer> fibRange = fib.getRange();
        final int maxPercents = 100;
        int amountOfOdd = 0;
        int amountOfEven = 0;
        double percentsOfOdd;
        double percentOfEven;
        Iterator<Integer> it = fibRange.iterator();
        while (it.hasNext()) {
            if ((it.next() % 2) != 0) {
                amountOfOdd++;
            } else {
                amountOfEven++;
            }
        }
        percentsOfOdd = ((double) amountOfOdd / (amountOfOdd
                + amountOfEven)) * maxPercents;
        percentOfEven = ((double) amountOfEven / (amountOfOdd
                + amountOfEven)) * maxPercents;
        menu.printPercentageFibonacciOddEven(percentsOfOdd, percentOfEven);
    }



}
